# Ud7-Ejemplo3
_Ejemplo 3 de la Unidad 7._

Vamos a implementar una aplicación que muestrará un calendario y un reloj y obtendremos sus valores seleccionados. Haremos uso de las clases _DatePickerDialog_ y _TimePickerDialog_ 
respectivamente.

Para ello tenemos que seguir los siguientes pasos:

## Paso 1: Creación del _activity_main.xml_

El primer paso será crear el _layout_ de la actividad principal, en la que tendremos dos botones, uno para mostrar el calendario y otro para el reloj, y varios _TextView_ para 
mostrar la fecha y hora seleccionada.
```html
<androidx.constraintlayout.widget.ConstraintLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <TextView
        android:id="@+id/fecha"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/fecha"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintHorizontal_bias="0.351"
        app:layout_constraintLeft_toLeftOf="parent"
        app:layout_constraintRight_toRightOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.294" />

    <TextView
        android:id="@+id/fechaElegida"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:layout_constraintBottom_toBottomOf="@+id/fecha"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.16"
        app:layout_constraintStart_toEndOf="@+id/fecha"
        app:layout_constraintTop_toTopOf="@+id/fecha"
        tools:text="25/12/2021" />

    <TextView
        android:id="@+id/hora"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/hora"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.363"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/fecha"
        app:layout_constraintVertical_bias="0.119" />

    <TextView
        android:id="@+id/horaElegida"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:layout_constraintBottom_toBottomOf="@+id/hora"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.126"
        app:layout_constraintStart_toEndOf="@+id/hora"
        app:layout_constraintTop_toTopOf="@+id/hora"
        app:layout_constraintVertical_bias="0.0"
        tools:text="21:50" />

    <Button
        android:id="@+id/botonFecha"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/boton_fecha"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.256"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/hora"
        app:layout_constraintVertical_bias="0.391" />

    <Button
        android:id="@+id/botonHora"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/boton_hora"
        app:layout_constraintBottom_toBottomOf="@+id/botonFecha"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toEndOf="@+id/botonFecha"
        app:layout_constraintTop_toTopOf="@+id/botonFecha" />

</androidx.constraintlayout.widget.ConstraintLayout>

```
## Paso 2: Creación de los _Fragments_

Crearemos las dos clases de _Pickers_ extendiendo de _DialogFragment_:

### _DatePickerFragment.java_
```java
public class DatePickerFragment extends DialogFragment {

    private DatePickerDialog.OnDateSetListener listener;

    // Creamos el Fragment y le asignamos el Listener
    @NonNull
    public static DatePickerFragment nuevaInstancia(DatePickerDialog.OnDateSetListener listener) {
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setListener(listener);

        return fragment;
    }

    public void setListener(DatePickerDialog.OnDateSetListener listener) {
        this.listener = listener;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Usamos la fecha actual en el DatePickerDialog
        Calendar calendario = Calendar.getInstance();

        int anyo = calendario.get(Calendar.YEAR);
        int mes = calendario.get(Calendar.MONTH);
        int dia = calendario.get(Calendar.DAY_OF_MONTH);

        // Creamos una nueva instancia de DatePickerDialog y la devolvemos
        return new DatePickerDialog(getActivity(), listener, anyo, mes, dia);
    }
}
```

### _TimePickerFragment.java_
```java
public class TimePickerFragment extends DialogFragment  {

    private TimePickerDialog.OnTimeSetListener listener;

    // Creamos el Fragment y le asignamos el Listener
    @NonNull
    public static TimePickerFragment nuevaInstancia(TimePickerDialog.OnTimeSetListener listener) {
        TimePickerFragment fragment = new TimePickerFragment();
        fragment.setListener(listener);

        return fragment;
    }

    public void setListener(TimePickerDialog.OnTimeSetListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Usamos la hora actual en el TimePickerDialog
        Calendar calendario = Calendar.getInstance();

        int hora = calendario.get(Calendar.HOUR_OF_DAY);
        int min = calendario.get(Calendar.MINUTE);

        // Creamos una nueva instancia de TimePickerDialog y la devolvemos
        return new TimePickerDialog(getActivity(), listener, hora, min, DateFormat.is24HourFormat(getActivity()));
    }
}
```

## Paso 4: Creación de la clase _MainActivity_

En ella crearemos los _Fragments_ y mostraremos la fecha y hora seleccionadas.
```java
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView fecha = findViewById(R.id.fechaElegida);
        TextView hora = findViewById(R.id.horaElegida);

        Button botonFecha = findViewById(R.id.botonFecha);
        Button botonHora = findViewById(R.id.botonHora);

        botonFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               DatePickerFragment fragment = DatePickerFragment.nuevaInstancia(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        fecha.setText(day + "/" + month + "/" + year);
                    }
                });

                fragment.show(getSupportFragmentManager(), "datePicker");
            }
        });

        botonHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerFragment fragment = TimePickerFragment.nuevaInstancia(new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int min) {
                        hora.setText(hour + ":" + min);
                    }
                });

                fragment.show(getSupportFragmentManager(), "timePicker");
            }
        });

    }
}
```
