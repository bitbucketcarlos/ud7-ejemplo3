package com.example.ud7_ejemplo3;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;

import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

public class DatePickerFragment extends DialogFragment {

    private DatePickerDialog.OnDateSetListener listener;

    // Creamos el Fragment y le asignamos el Listener
    @NonNull
    public static DatePickerFragment nuevaInstancia(DatePickerDialog.OnDateSetListener listener) {
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setListener(listener);

        return fragment;
    }

    public void setListener(DatePickerDialog.OnDateSetListener listener) {
        this.listener = listener;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Usamos la fecha actual en el DatePickerDialog
        Calendar calendario = Calendar.getInstance();

        int anyo = calendario.get(Calendar.YEAR);
        int mes = calendario.get(Calendar.MONTH);
        int dia = calendario.get(Calendar.DAY_OF_MONTH);

        // Creamos una nueva instancia de DatePickerDialog y la devolvemos
        return new DatePickerDialog(getActivity(), listener, anyo, mes, dia);
    }
}