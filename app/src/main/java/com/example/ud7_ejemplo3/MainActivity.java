package com.example.ud7_ejemplo3;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView fecha = findViewById(R.id.fechaElegida);
        TextView hora = findViewById(R.id.horaElegida);

        Button botonFecha = findViewById(R.id.botonFecha);
        Button botonHora = findViewById(R.id.botonHora);

        botonFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               DatePickerFragment fragment = DatePickerFragment.nuevaInstancia(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        fecha.setText(day + "/" + (month + 1) + "/" + year);
                    }
                });

                fragment.show(getSupportFragmentManager(), "datePicker");
            }
        });

        botonHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerFragment fragment = TimePickerFragment.nuevaInstancia(new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int min) {
                        hora.setText(hour + ":" + min);
                    }
                });

                fragment.show(getSupportFragmentManager(), "timePicker");
            }
        });

    }
}