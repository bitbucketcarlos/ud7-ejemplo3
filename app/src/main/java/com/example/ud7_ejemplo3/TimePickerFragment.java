package com.example.ud7_ejemplo3;

import android.app.Dialog;
import android.app.TimePickerDialog;

import android.os.Bundle;
import android.text.format.DateFormat;

import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

public class TimePickerFragment extends DialogFragment  {

    private TimePickerDialog.OnTimeSetListener listener;

    // Creamos el Fragment y le asignamos el Listener
    @NonNull
    public static TimePickerFragment nuevaInstancia(TimePickerDialog.OnTimeSetListener listener) {
        TimePickerFragment fragment = new TimePickerFragment();
        fragment.setListener(listener);

        return fragment;
    }

    public void setListener(TimePickerDialog.OnTimeSetListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Usamos la hora actual en el TimePickerDialog
        Calendar calendario = Calendar.getInstance();

        int hora = calendario.get(Calendar.HOUR_OF_DAY);
        int min = calendario.get(Calendar.MINUTE);

        // Creamos una nueva instancia de TimePickerDialog y la devolvemos
        return new TimePickerDialog(getActivity(), listener, hora, min, DateFormat.is24HourFormat(getActivity()));
    }
}